﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace Counter
{
    class StoryFormed
    {
        
        
        public static List<TableRow> SubMain (string ExcelfileName)
        {
            List<TableRow> Work = ParsData(ExcelfileName);
            List<List<int>> CountData=ParsRate(ExcelfileName);
            Work = CountDay(Work);
            Work = CountRate(Work,CountData);
            return Work;
        }
        static List<TableRow> ParsData(string ExcelfileName)
        {
            List<TableRow> ParsingList = new List<TableRow>();

            var objExcel = new Excel.Application();
            var objWorkbook = objExcel.Workbooks.Open(ExcelfileName); 
            var objWorksheet = objWorkbook.Sheets[1]; 
            int iLastRow = objWorksheet.Cells[objWorksheet.Rows.Count, "A"].End[Excel.XlDirection.xlUp].Row;
            var AData = (object[,])objWorksheet.Range["A2:A" + iLastRow].Value;
            var BData = (object[,])objWorksheet.Range["B2:B" + iLastRow].Value;
            var CData = (object[,])objWorksheet.Range["C2:C" + iLastRow].Value;

            for (int i = 1; i <= AData.GetUpperBound(0); i++)
            {
                TableRow z = new TableRow();
                z.Name = AData[i, 1].ToString();
                z.DateStart = Convert.ToDateTime(BData[i, 1]);
                if (CData[i, 1]==null)
                {
                    z.DateEnd = DateTime.Now;
                }
                else
                {
                    z.DateEnd = Convert.ToDateTime(CData[i, 1]);
                }
                
                ParsingList.Add(z);
            }

            objWorkbook.Close(true);
            objExcel.Quit();

            return ParsingList;
        }
        static List<List<int>> ParsRate(string ExcelfileName)
        {
            List<int> MinBorderRate = new List<int>();
            List<int> MaxBorderRate = new List<int>();
            List<int> Rate = new List<int>();
            var objExcel = new Excel.Application();
            var objWorkbook = objExcel.Workbooks.Open(ExcelfileName);
            var objWorksheet = objWorkbook.Sheets[2];
            int iLastRow = objWorksheet.Cells[objWorksheet.Rows.Count, "A"].End[Excel.XlDirection.xlUp].Row;
            var BData = (object[,])objWorksheet.Range["B2:B" + iLastRow].Value;
            var CData = (object[,])objWorksheet.Range["C2:C" + iLastRow].Value;
            var DData = (object[,])objWorksheet.Range["D2:D" + iLastRow].Value;
            for (int i = 1; i <= BData.GetUpperBound(0); i++)
            {
                MinBorderRate.Add(int.Parse(BData[i, 1].ToString()));
                if (CData[i, 1]==null)
                {
                    MaxBorderRate.Add(int.MaxValue);
                }
                else
                {
                    MaxBorderRate.Add(int.Parse(CData[i, 1].ToString()));
                }
                Rate.Add(int.Parse(DData[i, 1].ToString()));
            }
            objWorkbook.Close(true);
            objExcel.Quit();

            List<List<int>> massiv = new List<List<int>> { MinBorderRate, MaxBorderRate, Rate };
            return massiv;
        }
        static List<TableRow> CountDay (List<TableRow> WorkingList)
        {
            foreach (var item in WorkingList)
            {
                if ((item.DateEnd - item.DateStart).TotalHours % 24==0)
                {

                    item.DayCount = (int)(item.DateEnd - item.DateStart).TotalHours / 24;
                }
                else
                {
                    item.DayCount = (int)(item.DateEnd - item.DateStart).TotalHours / 24+1;
                }
                
            }
            return WorkingList;
        }
        static List<TableRow> CountRate(List<TableRow> WorkingList, List<List<int>> CountData)
        {
            List<TableRow> WorkList = new List<TableRow>();
            List<int> MinBorderRate = CountData[0];
            List<int> MaxBorderRate = CountData[1];
            List<int> Rate = CountData[2];
            foreach (var item in WorkingList)
            {
                int RateMoney = 0;
                int RateNomber = 0;
                int Daycheker=0;
                int check = item.DayCount;
                while (check > 0)
                {
                   
                    for (int i = 0; i < MinBorderRate.Count; i++)
                    {
                        if (check >= MinBorderRate[i]&& check <= MaxBorderRate[i])
                        {
                            if (RateNomber!=0 && i==RateNomber)
                            {
                                RateMoney += Rate[i];
                                Daycheker++;
                                check--;
                            }
                            else if (RateNomber != 0 && i != RateNomber)
                            {
                                TableRow now = new TableRow();
                                now.Name = item.Name;
                                now.DateStart = item.DateStart;
                                now.DateEnd = item.DateEnd;
                                now.DayCount = Daycheker;
                                now.Rate = Rate[i+1];
                                now.Money = RateMoney;
                                WorkList.Add(now);
                                Daycheker = 0;
                                RateMoney = 0;
                                RateNomber = 0;
                            }
                            else if (RateNomber == 0&&check==1)
                            {
                                TableRow now = new TableRow();
                                now.Name = item.Name;
                                now.DateStart = item.DateStart;
                                now.DateEnd = item.DateEnd;
                                now.DayCount = Daycheker+1;
                                now.Rate = Rate[i + 1];
                                now.Money = RateMoney+Rate[i];
                                WorkList.Add(now);
                                Daycheker = 0;
                                RateMoney = 0;
                                RateNomber = 0;
                                check--;
                            }
                            else if (RateNomber==0)
                            {
                                RateNomber = i;
                                RateMoney += Rate[i];
                                Daycheker++;
                                check--;
                            }

                        }
                        
                       
                    }
                   
                }
            }
            return WorkList;
        }
    }
}
