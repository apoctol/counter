﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Counter
{
    class DUI
    {
        public static List<DateTime> FormDataList(List<TableRow> CurrentList)
        {
            List<DateTime> NewData = new List<DateTime>();
            DateTime MinDate = new DateTime();
            DateTime MaxDate = new DateTime();
            MinDate = DateTime.Now;
            for (int i = 0; i < CurrentList.Count; i++)
            {
                if (CurrentList[i].DateStart<MinDate)
                {
                    MinDate = CurrentList[i].DateStart;
                }
                if (CurrentList[i].DateStart>MaxDate)
                {
                    MaxDate = CurrentList[i].DateStart;
                }
            }
            while (MinDate<=MaxDate)
            {
                NewData.Add(MinDate);
                MinDate = MinDate.AddDays(1);
            }
            return NewData;
        }
        public static List<TableRow> SortData(List<TableRow> CurrentList, DateTime MinDate, DateTime MaxDate)
        {
            List<TableRow> WorkList = new List<TableRow>();
            foreach (var item in CurrentList)
            {
                if (item.DateStart>=MinDate&&item.DateStart<=MaxDate)
                {
                    WorkList.Add(item);
                }
            }
            return WorkList;
        }
        public static int BillUser(List<TableRow> CurrentList, TableRow CurrentUser)
        {
            int a = 0;
            
            foreach (var item in CurrentList)
            {
                if (item.Name.Equals(CurrentUser.Name))
                {
                    a += item.Money;
                }
            }
            
            return a;
        }
       public static int BillTotal(List<TableRow> CurrentList)
        {
            int a = 0;
            foreach (var item in CurrentList)
            {
                a += item.Money;
            }
            return a;
        }
        
        //Кнопка итого выводящая сумму со всех ячеек в массиве.
    }
}
