﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Counter
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static List<TableRow> GlobData;
        public static string path;
        DateTime DateStart;
        DateTime DateEnd;
        List<TableRow> WorkData;
        public MainWindow()
        {
            InitializeComponent();
            GlobData = StoryFormed.SubMain(path);
            StartCombo.ItemsSource = DUI.FormDataList(GlobData);
            EndCombo.ItemsSource = DUI.FormDataList(GlobData);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (DateStart<DateEnd)
            {
                WorkData = DUI.SortData(GlobData, DateStart, DateEnd);
                grid.ItemsSource = WorkData;
            }
            else
            {
                MessageBox.Show("Выбран не правильный формат даты");
            }

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            int total = DUI.BillTotal(WorkData);
            MessageBox.Show("Итоговый счёт по контрактам за расчётный период составляет: " + total);
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;
            DateStart = Convert.ToDateTime(comboBox.SelectedValue);
        }

        private void ComboBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;
            DateEnd = Convert.ToDateTime(comboBox.SelectedValue);
        }

        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            TableRow ChangeValue = grid.SelectedItem as TableRow;
            
            if (ChangeValue == null)
            {
                MessageBox.Show("Данные не загружены");
            }
            else
            { 
                int result = DUI.BillUser(WorkData, ChangeValue);
                MessageBox.Show("Итоговый счёт по контракту " + ChangeValue.Name + " Составляет: " + result);
            }
        }

        private void Button_Click_2(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.Filter = "Данные (*.xlsx)|*.xlsx";

            if (openFileDialog.ShowDialog() == true)
            {
                FileInfo fileInfo = new FileInfo(openFileDialog.FileName);
                path = fileInfo.FullName;
                return;
            }
            
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            StartWindow taskWindow = new StartWindow();
            taskWindow.Show();
            Close();
        }

    }
}
